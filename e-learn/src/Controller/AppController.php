<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Http\Response;

class AppController extends Controller
{

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        $this->loadComponent('Auth',[
            'loginRedirect' => ['controller' => 'Home', 'action' => 'index'],
            'logoutRedirect' => ['controller' => 'Login' ,'action' => 'login'],
            'authError' => 'Bạn chưa được cấp quyền truy cập trang .Vui lòng đăng nhập bằng tài khoản hợp lệ '
        ]);
        $this->Auth->allow(['display','view','login','register']);
        $this->loadComponent('Paginator');
    }

    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function isAuthorized($user){
        return false;
        if(isset($user['role']) && $user['role'] == 1 )
            return true;
    }

    public $paginate = ['limit' => '5'];

}