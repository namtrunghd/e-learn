<?php
namespace App\Controller\Admin;
use Cake\ORM\TableRegistry;

class UsersController extends AdminController
{

    public function initialize()
    {
        parent::initialize();
    }
    public function index()
    {
        $usersTable = TableRegistry::get('Users');
        $users = $usersTable->find()->order(['id' => 'DESC']);
        $this->set('users',$users);
        $this->paginate($users);
    }

    public function view($id = null){
       $user = $this->Users->get($id,
            ['contain' => ['comments','answers']]
            );
        $this->set('user',$user);
    }

    /**
     * @return \Cake\Http\Response|null
     */
    public function add(){
            $user = $this->Users->newEntity();
            if($this->request->is('post')){
                $this->Users->patchEntity($user,$this->request->getData());
                if($this->Users->save($user)){
                    $this->Flash->success(__('Lưu thành công'));
                    return $this->redirect(['action' => 'index']);
                }else{
                    $this->Flash->error(__('Thêm mới thất bại. Vui lòng kiểm tra lại thông tin'));
                }
            }
            $this->set(compact('user'));
            $this->set('_serialize', ['user']);
    }

    public function edit($id = null)
    {
        $user = $this->Users->get($id,
            ['contain' => []]
            );
        if($this->request->is(['post','patch','put'])){
            $this->Users->patchEntity($user,$this->request->getData());
            if($this->Users->save($user)){
                $this->Flash->success('Thông tin của " '. $user['username'] .' " đã được thay dổi');
                return $this->redirect(['action'=>'index']);
            }else{
                $this->Flash->error('Các thay đổi chưa được lưu. Vui lòng kiểm tra thông tin và thử lại');
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function delete($id = null){
        $user = $this->Users->get($id);
        $this->request->allowMethod(['post','delete']);
        if($this->Users->delete($user)){
            $this->Flash->success('Đã xóa tài khoản '.$user['username']);
            $this->redirect(['action' => 'index']);
        }else{
            $this->Flash->error('Có lỗi xảy ra, không thể xóa tài khoản này');
        }

    }


}
