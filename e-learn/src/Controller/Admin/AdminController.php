<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

class AdminController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('admin');
        //kiem tra dang nhap
        if ($this->request->session()->read('Auth.User')) {
            //kiem tra quyen quan tri
            if ($this->request->session()->read('Auth.User.role') != 1) {
                return $this->redirect(['controller' => '..\Home', 'action' => 'index']);
            }
        } else {
            return $this->redirect(['controller' => '..\Login', 'action' => 'index']);
        }
    }

    public function index()
    {
        $this->render('..\Home\index');


    }


}
