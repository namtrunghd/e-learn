<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use Cake\Routing\Router;
use Cake\Controller\Component\FlashComponent;

class PostsController extends AppController
{

    public function initialize()
    {
        $this->viewBuilder()->setLayout('admin');
    }
    public function index()
    {
        $posts = TableRegistry::get('Posts')->find('all');
//        pr($posts);die;
        $this->set(compact('posts'));
        $this->set('_serialize', ['posts']);
    }

    public function add(){
        $posts = $this->Posts->newEntity();

        if ($this->request->is('post')){
//            pr($this->request->data);die;
            $posts = $this->Posts->patchEntity($posts,$this->request->getData());
            $posts->slug = strtolower(Text::slug($posts->title,'-'));
//            pr($posts);die;
            if ($this->Posts->save($posts)){
//                $this->Flash->success(__('Thêm bài viết thành công.'));
                return $this->redirect(['action'=>'index']);
            }
//            $this->Flash->error('Not add your posts');
        }
        $this->set('posts',$posts);
        $this->set('_serialize', ['post']);
    }
    public function edit($id = null)
    {
        $post = $this->Posts->get($id);
        if ($this->request->is(['put','patch','post'])){
            $post = $this->Posts->patchEntity($post,$this->request->getData());
            if ($this->Posts->save($post)){
                return $this->redirect(['action'=>'index']);
            }
        }
        $this->set('posts',$post);
        $this->set('_serialize',['post']);
    }



    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $post = $this->Posts->get($id);
        if ($this->Posts->delete($post)) {
            return $this->redirect(['action' => 'index']);
        }

    }



}
