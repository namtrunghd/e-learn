<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Sửa bài viết
                </h1>

            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="container">
                <div class="col-md-6">

                    <?php echo $this->Form->create($posts, ['type' => 'file']); ?>
                    <?php echo $this->Form->hidden('id'); ?>

                    <div class="form-group">
                        <label>Tiêu đề</label>
                        <?php echo $this->Form->text('title', ['class' => 'form-control']); ?>
                    </div>

                    <div class="form-group">
                        <label>Mô tả</label>
                        <?php echo $this->Form->text('description', ['class' => 'form-control']); ?>
                    </div>

                    <div class="form-group">
                        <label>Nội dung</label>
                        <?php echo $this->Form->textarea('content',['class'=>'ckeditor']);?>
                    </div>
                    <div class="form-group">
                        <label>Ảnh minh họa</label>
                        <input type="file">
                    </div>
                    <div class="form-group">
                        <label>Trạng thái</label>
                        <?php
                        echo $this->Form->radio('status', ['Active','Disable'],['class'=>'radio-inline']);
                        ?>
                    </div>
                    <button type="submit" class="btn btn-success">Sửa bài viết</button>

                    <?php echo $this->Form->end();?>

                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
