<?php echo $this->Flash->render() ?>

<div id="page-wrapper">
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-8">
                    <h1 class="page-header">
                        Quản lý bài viết
                    </h1>
                </div>
                <div class="pull-right">
                    <?php echo $this->Html->link(
                        '<i class="fa fa-pencil"></i> Thêm bài viết mới',
                        '/admin/posts/add/',
                        ['class' => 'btn btn-success btn-md','escape'=>false]
                    );?>
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-md-12">

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tiêu đề</th>
                                <th>Hình ảnh</th>
                                <th>Trạng thái</th>
                                <th>Ngày tạo</th>
                                <th>Ngày sửa</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $stt=1;?>
                            <?php foreach ($posts as $post):?>

                            <tr>
                                <td><?php echo $stt;?></td>
                                <td><?php echo $post['title']?></td>
                                <td><?php echo $post['thumbnail']?></td>
                                <td><?php echo $post['status']?></td>
                                <td><?php echo $post['created']?></td>
                                <td><?php echo $post['modified']?></td>
                                <td>
                                    <?php echo $this->Html->link(
                                        '<i class="fa fa-pencil"></i> Edit',
                                        '/admin/posts/edit/'.$post['id'],
                                        ['class' => 'btn btn-info btn-xs','escape'=>false]
                                    );?>

                                    <?php
                                    echo $this->Form->postLink('<i class="fa fa-trash-o"></i> Delete',
                                        ['action' => 'delete/'.$post['id']],
                                        ['class' => 'btn btn-danger btn-xs','escape'=>false,'confirm'=>'Are you sure?'],
                                        []);
                                    ?>
                                </td>
                            </tr>
                            <?php $stt++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

