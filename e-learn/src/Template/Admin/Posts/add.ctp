<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Thêm bài viết
                </h1>

            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="container">
                <div class="col-md-6">

                    <?php echo $this->Form->create($posts, ['type' => 'file']); ?>

                    <div class="form-group">
                        <label>Tiêu đề</label>
                        <?php echo $this->Form->text('title', ['class' => 'form-control','required'=>false]); ?>
                        <?php if ($this->Form->isFieldError('title')): ?>
                            <span style="color: red;"><?php echo $this->Form->error('title'); ?></span>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <label>Mô tả</label>
                        <?php echo $this->Form->text('description', ['class' => 'form-control','required'=>false]); ?>
                        <?php if ($this->Form->isFieldError('description')): ?>
                            <span style="color: red;"><?php echo $this->Form->error('description'); ?></span>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <label>Nội dung</label>
                        <?php echo $this->Form->textarea('content',['class'=>'ckeditor']);?>
                        <?php if ($this->Form->isFieldError('content')): ?>
                            <span style="color: red;"><?php echo $this->Form->error('content'); ?></span>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <label>Ảnh minh họa</label>
<!--                        <input type="file">-->
                        <?php echo $this->Form->file('thumbnail', ['required' => false, 'class' => 'form-control', 'label' => false]
                        ); ?>
                        <?php if ($this->Form->isFieldError('thumbnail')): ?>
                            <span style="color: red;"><?php echo $this->Form->error('thumbnail'); ?></span>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <label>Trạng thái</label>
                        <?php
                        echo $this->Form->radio('status', ['Active','Disable'],['class'=>'radio-inline']);
                        ?>
                    </div>
                    <button type="submit" class="btn btn-success">Thêm bài viết</button>

                    <?php echo $this->Form->end();?>

                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->
