<style>
    .col-md-4, h1{
        color: purple;
    }
    .col-md-8{
        margin-bottom :10px;
    }
    input {
        border-radius : 5px;
        border-shadow : 0px;
        height: 34px;width:100%;
    }
</style>

<div id="page-wrapper">
<span style='color:red;'><?= $this->Flash->render(); ?></span>
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Thêm mới người dùng
                </h1>

            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="container">

                 <?php echo $this->Form->create($user); ?>

                 <div class="col-md-6">

<!-- Username  --> <div class="form-group">
                        <div class='col-md-4'>Tên đăng nhập</div>
                        <div class='col-md-8'>
                            <?php echo $this->Form->text('username',['class' => 'form-control', 'required']) ; ?>
                            <?php if ($this->Form->isFieldError('username')): ?>
                                <span style="color: red;"><?php echo $this->Form->error('username'); ?></span>
                            <?php endif; ?>
                        </div>
                    </div>

<!-- Password  --> <div class="form-group">
                         <div class='col-md-4'>Mật khẩu</div>
                         <div class='col-md-8'>
                             <?php echo $this->Form->password('password', ['class' => 'form-control','required'=>true,'name' => 'password']); ?>
                             <?php if ($this->Form->isFieldError('password')): ?>
                                 <span style="color: red;"><?php echo $this->Form->error('password'); ?></span>
                             <?php endif; ?>
                         </div>
                    </div>

<!--ConfirmPass --> <div class="form-group">
                         <div class='col-md-4'>Xác nhận mật khẩu</div>
                         <div class='col-md-8'>
                            <input type='password' name='confirm_password' class='form-control'  />
                         </div>
                    </div>


<!-- Fullname -->   <div class="form-group">
                        <div class='col-md-4'>Họ và tên </div>
                        <div class='col-md-8'>
                            <?php echo $this->Form->text('full_name', ['class' => 'form-control','required'=>false]); ?>
                            <?php if ($this->Form->isFieldError('full_name')): ?>
                                <span style="color: red;"><?php echo $this->Form->error('full_name'); ?></span>
                            <?php endif; ?>
                        </div>
                    </div>

<!-- Phone -->      <div class="form-group">
                        <div class='col-md-4'>Phone</div>
                        <div class='col-md-8'>
                            <?php echo $this->Form->tel('phone',['class'=>'form-control']);?>
                            <?php if ($this->Form->isFieldError('phone')): ?>
                                <span style="color: red;"><?php echo $this->Form->error('phone'); ?></span>
                            <?php endif; ?>
                        </div>
                    </div>

                </div>
                <!-- end col-md-6 -->

                <div class='col-md-6'>

<!-- Email -->      <div class="form-group">
                        <div class='col-md-4'>Email</div>
                        <div class='col-md-8'>
                            <?php echo $this->Form->email('email',['class'=>'form-control']);?>
                            <?php if ($this->Form->isFieldError('email')): ?>
                                <span style="color: red;"><?php echo $this->Form->error('email'); ?></span>
                            <?php endif; ?>
                        </div>
                    </div>

<!-- Role -->      <div class="form-group">
                        <div class='col-md-4'>Vai trò</div>
                        <div class='col-md-8'>
                            <?php echo $this->Form->number('role',['class'=>'form-control','min'=> '1','max' => '5']);?>
                            <?php if ($this->Form->isFieldError('role')): ?>
                                <span style="color: red;"><?php echo $this->Form->error('role'); ?></span>
                            <?php endif; ?>
                        </div>
                    </div>

<!-- Type -->      <div class="form-group">
                        <div class='col-md-4'>Loại tài khoản</div>
                        <div class='col-md-8'>
                            <?php echo $this->Form->number('type',['class'=>'form-control','required' =>'required','min'=> '1','max' => '5']);?>
                            <?php if ($this->Form->isFieldError('type')): ?>
                                <span style="color: red;"><?php echo $this->Form->error('type'); ?></span>
                            <?php endif; ?>
                        </div>
                    </div>

<!-- Level -->      <div class="form-group">
                        <div class='col-md-4'>Cấp độ quản trị</div>
                        <div class='col-md-8'>
                            <?php echo $this->Form->number('level',['class'=>'form-control','required' => 'required','min'=> '1','max' => '5']);?>
                            <?php if ($this->Form->isFieldError('level')): ?>
                                <span style="color: red;"><?php echo $this->Form->error('level'); ?></span>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class='col-md-4'>
                        </div>
                        <div class='col-md-8'>
                            <button type="submit" class="btn btn-success" style='color:purple'>Lưu</button>
                        </div>
                    </div>
                </div>
                <!-- end col-md-6 -->

                <?php echo $this->Form->end();?>
                <!-- end form -->

            </div>

        </div>
        <!-- /.row -->

<?= $this->element('Admin\admin_user_bottom'); ?>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->



