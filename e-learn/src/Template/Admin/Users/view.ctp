<style>
    .col-md-4, h1{
        color: purple;
    }
    .col-md-8{
        margin-bottom :10px;
    }
    input {
        border-radius : 5px;
        border-shadow : 0px;
        height: 34px;width:100%;
    }
</style>

<div id="page-wrapper">
<?= $this->Flash->render(); ?>
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                   Thông tin tài khoản : <?= $user['username']; ?>
                </h1>

            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="container">

                <div class = 'col-md-6s'>
<!-- Role -->      <div class="form-group">
                       <h3><?php echo 'Thông tin '; if($user['role'] == 1) echo "quản trị viên"; else echo "thành viên"; ?></h3>
                    </div>
                </div>

                 <div class="col-md-6">

<!-- Username  --> <div class="form-group">
                        <div class='col-md-4'>Tên đăng nhập</div>
                        <div class='col-md-8'><?= ': '.$user['username']; ?></div>
                    </div>

<!-- Fullname -->   <div class="form-group">
                        <div class='col-md-4'>Họ và tên </div>
                        <div class='col-md-8'><?= ': '.$user['full_name']; ?></div>
                    </div>

<!-- Phone -->      <div class="form-group">
                        <div class='col-md-4'>Số điện thoại liên hệ</div>
                        <div class='col-md-8'><?= ': '.$user['phone']; ?></div>
                    </div>

<!-- Email -->      <div class="form-group">
                        <div class='col-md-4'>Email</div>
                        <div class='col-md-8'><?= ': '.$user['email']; ?></div>
                    </div>

                </div>
                <!-- end col-md-6 -->

                <div class='col-md-6'>

<!-- Type -->      <div class="form-group">
                        <div class='col-md-4'>Loại tài khoản</div>
                        <div class='col-md-8'><?= ': '.$user['type']; ?></div>
                    </div>

<!-- Level -->      <div class="form-group">
                        <div class='col-md-4'>Cấp độ quản trị</div>
                        <div class='col-md-8'><?= ': '.$user['level']; ?></div>
                    </div>

<!-- Created  --> <div class="form-group">
                         <div class='col-md-4'>Đăng ký</div>
                         <div class='col-md-8'><?= ': '.$user['created']; ?></div>
                    </div>

<!--Modified --> <div class="form-group">
                         <div class='col-md-4'>Chỉnh sửa thông tin</div>
                         <div class='col-md-8'><?= ': '. $user['modified']; ?></div>
                    </div>

            </div>
                <!-- end col-md-6 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

<?= $this->element('Admin\admin_user_bottom'); ?>

</div>
<!-- /#page-wrapper -->


