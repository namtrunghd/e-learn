<div id="page-wrapper">
<span style='color:red;'><?php echo $this->Flash->render() ?></span>
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-8">
                    <h1 class="page-header">
                        Quản lý người dùng
                    </h1>
                </div>
                <div class="pull-right">
                    <?php echo $this->Html->link(
                        '<i class="fa fa-plus"></i> Thêm mới người dùng',
                        '/admin/users/add/',
                        ['class' => 'btn btn-success btn-md','escape'=>false]
                    );?>
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-md-12">

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tài khoản</th>
                                <th>Họ và tên</th>
                                <th>Email</th>
                                <th>Vai trò</th>
                                <th>Cấp</th>
                                <th>Đăng ký</th>
                                <th>Cập nhật</th>
                                <th>Quản lý</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $stt = 1 ;?>
                            <?php foreach ($users as $user):?>

                            <tr>
                                <td><?php echo $stt ;?></td>
                                <td><?php echo $user['username']?></td>
                                <td><?php echo $user['full_name']?></td>
                                <td><?php echo $user['email']?></td>
                                <td><?php if(  $user['role'] == 1 ) echo 'Quản trị viên'; else echo "Thành viên" ;  ?></td>
                                <td><?php echo $user['level']?></td>
                                <td><?php echo $user['created']?></td>
                                <td><?php echo $user['modified']?></td>
                                <td>
                                    <?php echo $this->Html->link(
                                        '<i class="fa fa-eye"></i> Xem',
                                        '/admin/users/view/'.$user['id'],
                                        ['class' => 'btn btn-info btn-xs','escape'=>false]
                                    );?>

                                    <!-- quản trị viên cấp độ  >> 1 -->
                                    <?php if( $this->request->session()->read('Auth.User.role') == 1 ) : ?>
                                         <?php echo $this->Html->link(
                                              '<i class="fa fa-pencil"></i> Sửa',
                                              '/admin/users/edit/'.$user['id'],
                                              ['class' => 'btn btn-primary btn-xs','escape'=>false]
                                         );?>
                                    <?php endif; ?>

                                    <!-- nếu là quản  trị viên cấp 1 mới được sửa và xóa -->
                                    <?php if( $this->request->session()->read('Auth.User.role') == 1 && $this->request->session()->read('Auth.User.level') == 1 ) { ?>
                                        <?php
                                        echo $this->Form->postLink('<i class="fa fa-trash-o"></i> Xóa',
                                            ['action' => 'delete/'.$user['id']],
                                            ['class' => 'btn btn-danger btn-xs','escape'=>false,'confirm'=>'Bạn có chắc muốn xóa :'.$user['username'] ],
                                            []);
                                        ?>
                                    <?php } ?>

                                </td>
                            </tr>
                            <?php $stt++; endforeach;?>
                            </tbody>
                        </table>

                        <!-- PHÂN TRANG -->
                            <?= $this->element('paginator'); ?>
                        <!-- / PHÂN TRANG -->

                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

