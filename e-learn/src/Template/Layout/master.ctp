<?php

?>
<!DOCTYPE html>
<html>
    <head>
      <?= $this->element('Client\master_head'); ?>
    </head>
    <body>
        <!-- Fixed navbar -->
            <?= $this->element('Client\master_navbar'); ?>
        <!-- /.navbar -->
        
        <!--content-->
            <?php echo $this->fetch('content');?>
        <!--/ content-->
        
        <footer id="footer">
            <?= $this->element('Client\master_footer') ?>
        </footer>

        <!-- JavaScript libs are placed at the end of the document so the pages load faster -->
        <?php echo $this->Html->script('/frontend/assets/js/modernizr-latest.js');?>
        <?php echo $this->Html->script('/frontend/assets/js/jquery.min.js');?>
        <?php echo $this->Html->script('/frontend/assets/js/fancybox/jquery.fancybox.pack.js');?>
        <?php echo $this->Html->script('/frontend/assets/js/jquery.mobile.customized.min.js');?>
        <?php echo $this->Html->script('/frontend/assets/js/jquery.easing.1.3.js');?>
        <?php echo $this->Html->script('/frontend/assets/js/camera.min.js');?>
        <?php echo $this->Html->script('/frontend/assets/js/bootstrap.min.js');?>
        <?php echo $this->Html->script('/frontend/assets/js/custom.js');?>

        <script>
            jQuery(function () {

                jQuery('#camera_wrap_4').camera({
                    transPeriod: 500,
                    time: 3000,
                    height: '600',
                    loader: 'false',
                    pagination: true,
                    thumbnails: false,
                    hover: false,
                    playPause: false,
                    navigation: false,
                    opacityOnGrid: false,
                    imagePath: 'frontend/assets/images/'
                });

            });

        </script>

    </body>
</html>
