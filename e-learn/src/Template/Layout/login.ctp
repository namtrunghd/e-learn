<?php

use Cake\View\Helper\HtmlHelper;
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset('utf-8') ?>
        <title>Đăng nhập/ Đăng ký E-learn </title>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <?= $this->Html->css('login_style') ?>
        <?= $this->Html->css('bootstrap.min') ?>
        <?= $this->Html->css('bootstrap') ?>
        <?= $this->Html->script('login_script') ?>
        <?= $this->Html->script('bootstrap') ?>
        <?= $this->Html->script('bootstrap.min') ?>
        <?= $this->Html->script('jquery') ?>
    </head>

    <body>
        <?= $this->fetch('content'); ?>
    </body>
</html>

