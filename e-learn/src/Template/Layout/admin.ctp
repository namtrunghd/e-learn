<?php
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->element('Admin\admin_head'); ?>
</head>
<style>
    h1{
        color:purple;
    }
</style>

<body>

<!-- #wrapper -->
<div id="wrapper">

    <!-- Navigation -->
    <?= $this->element('Admin\admin_navbar'); ?>
    <!-- / Navigation -->

    <!-- content -->
    <?php echo $this->fetch('content');?>
    <!-- / content  -->
</div>
<!-- /#wrapper -->

<!-- jQuery -->
<?php echo $this->Html->script('/backend/js/jquery.js');?>
<?php echo $this->Html->script('/backend/js/bootstrap.min.js');?>
<?php echo $this->Html->script('/backend/js/plugins/morris/raphael.min.js');?>
<?php echo $this->Html->script('/backend/js/plugins/morris/morris.min.js');?>
<?php echo $this->Html->script('/backend/js/plugins/morris/morris-data.js');?>
<?php echo $this->Html->script('../js/ckeditor/ckeditor.js');?>


</body>

</html>