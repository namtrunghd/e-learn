<?php ?>
<div class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <!-- Button for smallest screens -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            <a class="navbar-brand" href="index.html">
                <img src="frontend/assets/images/logo.png" alt="Techro HTML5 template"></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right mainNav">
                <!--<li class="active"><a href="index.html">Home</a></li>-->
                <?php if(!$this->request->session()->read('Auth.User')) { ?>
                    <li>
                        <?= $this->Html->link('Đăng nhập',['controller' => 'login', 'action' => 'login']) ?>
                    </li>
                <?php } ?>
                <li>
                    <?= $this->Html->link('Đăng ký',['controller' => 'Registry', 'action' => 'register']) ?>
                </li>
               <?php if($this->request->session()->read('Auth.User')) { ?>
                 <li>
                      <?= $this->Html->link('Đăng xuất',['controller' => 'login', 'action' => 'logout']) ?>
                 </li>
                <?php } ?>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</div>