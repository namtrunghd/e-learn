  <?php
   $projectDiscription = "E-learn Project";
   ?>
  <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= $projectDiscription ?> :
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>
        <link rel="favicon" href="frontend/assets/images/favicon.png">
        <!--<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">-->
  <?php echo $this->Html->css('/frontend/assets/css/bootstrap.min.css');?>
  <?php echo $this->Html->css('/frontend/assets/css/font-awesome.min.css');?>
  <?php echo $this->Html->css('/frontend/assets/css/bootstrap-theme.css');?>
  <?php echo $this->Html->css('/frontend/assets/css/style.css');?>
  <?php echo $this->Html->css('/frontend/assets/css/camera.css');?>
      <link rel='stylesheet' id='camera-css'  href='frontend/assets/css/camera.css' type='text/css' media='all'>


    <!--    <script src="assets/js/html5shiv.js"></script>-->
<!--    <script src="assets/js/respond.min.js"></script>-->
    <?php echo $this->Html->script('/frontend/assets/js/html5shiv.js');?>
    <?php echo $this->Html->script('/frontend/assets/js/respond.min.js');?>