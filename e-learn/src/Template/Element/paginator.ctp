<div class="paginator">
     <ul class="pagination">
          <?= $this->Paginator->first('<< ' . __('Trang đầu')) ?>
          <?= $this->Paginator->prev('< ' . __('Trang trước')) ?>
          <?= $this->Paginator->numbers() ?>
          <?= $this->Paginator->next(__('Trang tiếp theo') . ' >') ?>
          <?= $this->Paginator->last(__('Trang cuối cùng') . ' >>') ?>
     </ul>
     <p style='font-size:10;color:purple;'><?= $this->Paginator->counter(['format' => __('Trang {{page}}/ {{pages}}, đang hiển thị {{current}} / {{count}} bản ghi ')]) ?></p>
</div>