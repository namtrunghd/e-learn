<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">E-Learn Admin</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $this->request->session()->read('Auth.User.username');?> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <?php echo $this->Html->link(
                            '<i class="fa fa-fw fa-power-off">   </i> Log Out',
                            '/login/logout',['escape' => false]
                        );?>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="active">
                    <?php echo $this->Html->link(
                        '<i class="fa fa-fw fa-dashboard">   </i> Home',
                        ['controller' => 'Home', 'action' => 'index'],['escape' => false]
                    );?>
                </li>
                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-fw fa-edit">   </i> Posts',
                        ['controller' => 'Posts', 'action' => 'index'],['escape' => false]
                    );?>
                </li>
                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-fw fa-sitemap">   </i> Categories',
                        ['controller' => 'Categories', 'action' => 'index'],['escape' => false]
                    );?>
                </li>
                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-fw fa-users">   </i> Users',
                        ['controller' => 'Users', 'action' => 'index'],['escape' => false]
                    );?>
                </li>
                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-fw fa-comment-o">   </i> Comments',
                        ['controller' => 'Commnets', 'action' => 'index'],['escape' => false]
                    );?>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>