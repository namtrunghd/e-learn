   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Quản trị E-learn</title>

    <?php echo $this->Html->css('/backend/css/bootstrap.min.css');?>
    <?php echo $this->Html->css('/backend/css/sb-admin.css');?>
    <?php echo $this->Html->css('/backend/css/plugins/morris.css');?>
    <?php echo $this->Html->css('/backend/font-awesome/css/font-awesome.min.css');?>

    <?php echo $this->Html->script('/backend/js/html5shiv.js');?>
    <?php echo $this->Html->script('/backend/js/respond.min.js');?>

<!--    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>-->
<!--    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>-->
    <![endif]-->