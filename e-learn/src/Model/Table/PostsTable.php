<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Posts Model
 *
 * @method \App\Model\Entity\Post get($primaryKey, $options = [])
 * @method \App\Model\Entity\Post newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Post[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Post|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Post patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Post[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Post findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PostsTable extends Table
{
    public $tmpData;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('posts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

//        $validator
//            ->requirePresence('title', 'create')
//            ->notEmpty('title');
        $validator
            ->add('title', [
                'minLength' => [
                    'rule' => ['minLength', 10],
                    'last' => true,
                    'message' => 'Tối thiểu 10 ký tự.'
                ],
                'maxLength' => [
                    'rule' => ['maxLength', 250],
                    'message' => 'Tối đa 250 ký tự.'
                ]
            ]);


        $validator
            ->requirePresence('slug', 'create')
            ->notEmpty('slug');

        $validator
            ->add('thumbnail', [
                'uploadedFile' => [
                    'rule' => ['uploadedFile', ['optional' => true]],
                    'message' => 'Vui lòng chọn ảnh.'
                ],
                'extension' => [
                    'rule' => ['extension', ['jpg', 'png', 'mpg', 'mpeg', 'gif', 'bmp']],
                    'message' => 'Định dạng cho phép JPG, PNG.'
                ],
                'fileSize'=>[
                    'rule'=>['fileSize','<=','1MB'],
                    'message'=>'Kích thước ảnh không quá 1MB'
                ],
                'uploadFile' => [
                    'rule'=>['uploadFile'],
                    'message'=>'Error'
                ]
            ]);

        $validator
            ->notEmpty('description');

        $validator
            ->requirePresence('content', 'create')
            ->notEmpty('content');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }


    public function beforeValidate($options = []){
        if (!$this->isNewRecord()){
            if (empty($this->data[$this->alias]['thumbnail']['name'])){
                unset($this->data[$this->alias['thumbnail']]);
            }
        }
    }

    public function beforeDelete($cascade=true){
        $this->tmpData = $this->findById($this->id);
    }

    public function afterDelete(){
        $path = WWW_ROOT.$this->tmpData[$this->alias['thumbnail']];
        unlink($path);
    }



}
