<?php

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;


/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */


Router::defaultRouteClass(DashedRoute::class);

Router::prefix('admin', function ($routes) {
    $routes->connect('/:controller',['controller' => 'Home', 'action' => 'index']);
    $routes->connect('/:controller', ['controller' => 'Posts', 'action' => 'index']);
    $routes->connect('/:controller/:action/*', ['controller' => 'Posts', 'action' => 'add']);
    $routes->connect('/:controller/:action/:id', ['controller' => 'Posts', 'action' => 'edit']);
    $routes->connect('/:controller/:action/:id', ['controller' => 'Posts', 'action' => 'delete']);
    $routes->connect('/:controller', ['controller' => 'Categories', 'action' => 'index']);
    $routes->connect('/:controller', ['controller' => 'Comments', 'action' => 'index']);
    $routes->connect('/:controller', ['controller' => 'Users', 'action' => 'index']);

});

Router::scope('/', function (RouteBuilder $routes) {

    $routes->connect('/', ['controller' => 'Home', 'action' => 'index']);
    $routes->connect('/user/add', ['controller' => 'Users' , 'action' => 'add']);
    $routes->connect('/login', ['controller' => 'Login', 'action' => 'login']);
    $routes->connect('/logout', ['controller' => 'Login', 'action' => 'logout']);
    $routes->connect('/registry', ['controller' => 'Registry', 'action' => 'register']);

    $routes->fallbacks(DashedRoute::class);
});

Plugin::routes();
